//
//  ApiRouter.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import Foundation
import Alamofire

//The APIRouter helps to simplify the network request management
enum APIRouter: URLRequestConvertible {
    
    //APIRouter case we need to request the last 24h data about earthwuakes
    case last24hEarthquakes
    
    //HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .last24hEarthquakes:
            return .get
        }
    }
    
    //Path
    private var path: String {
        switch self {
        case .last24hEarthquakes:
            return "/summary/all_day.geojson"
        }
    }
    
    //Parameters (Add here the request params)
    private var parameters: Parameters? {
        return nil
    }
    
    //URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try NetworkServiceConstants.ProductionServer.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        //Add here the request headers
        urlRequest.httpHeaders = []
        
        // Parameters
        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
}

