//
//  ApiService.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import Foundation
import Alamofire

protocol ApiServiceProtocol {
    func getEarthquakes(route: APIRouter, completion: @escaping (Result<Earthquake>) -> Void)
}

struct ApiService: ApiServiceProtocol {
    
    /// Get all the Earthquakes object in the last 24h
    ///
    /// - Parameters:
    ///   - route: APIRouter case for the request
    ///   - completion: Generic Result that returns either Earthquake object or Error
    func getEarthquakes(route: APIRouter, completion: @escaping (Result<Earthquake>) -> Void) {
        AF.request(route).responseDecodable( decoder: JSONDecoder()) { (response: DataResponse<Earthquake>) in
            switch response.result {
            case .success(let value):
                completion(.success(value))
            case .failure(let error):
                completion(.error(error))
            }
        }
    }
}
