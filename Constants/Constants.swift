//
//  Constants.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

enum CoordinatorKeys {
    case earthquakeCoordinator
}

struct NetworkServiceConstants {
    struct ProductionServer {
        static let baseURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0"
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}

enum CellsIdentifiers {
    static let mainTableViewCell = "MainTableViewCell"
    static let earthquakeListTableViewCellIdentifier = "EarthquakeListTableViewCell"
}

enum MapAnnotationIdentifier {
    static let earthquakeMapAnnotation = "EarthquakeMapAnnotation"
}
