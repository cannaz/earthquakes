//
//  AppCoordinator.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import UIKit

final class AppCoordinator: Coordinator {
    
    private var window: UIWindow
    internal var childCoordinators: [CoordinatorKeys:Coordinator]
    internal var navigationController: UINavigationController
    
    public var rootViewController: UIViewController {
        return navigationController
    }
    
    init(in window: UIWindow) {
        self.childCoordinators = [:]
        self.navigationController = UINavigationController()
        self.navigationController.navigationBar.isHidden = false
        self.window = window
        self.window.backgroundColor = .white
        self.window.rootViewController = rootViewController
        self.window.makeKeyAndVisible()
    }
    
    public func start() {
        let earthquakeCoordinator = EarthquakeCoordinator(with: navigationController)
        addChild(coordinator: earthquakeCoordinator, with: .earthquakeCoordinator)
    }
}
