//
//  Coordinator.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import UIKit

protocol Coordinator: class {
    var navigationController: UINavigationController { get }
    var childCoordinators: [CoordinatorKeys:Coordinator] { get set }
    
    func start()
    func addChild(coordinator: Coordinator, with key: CoordinatorKeys)
    func removeChild(coordinator: Coordinator)
}

extension Coordinator {
    
    func addChild(coordinator: Coordinator, with key: CoordinatorKeys) {
        childCoordinators[key] = coordinator
    }
    
    func removeChild(coordinator: Coordinator) {
        childCoordinators = childCoordinators.filter { $0.value !== coordinator }
    }
    
}

extension Coordinator {
    
    func navigate(to viewController: UIViewController, with presentationStyle: NavigationStyle, animated: Bool = true) {
        switch presentationStyle {
        case .present:
            navigationController.present(viewController, animated: animated, completion: nil)
        case .push:
            navigationController.pushViewController(viewController, animated: true)
        }
    }
    
}

enum NavigationStyle {
    case present
    case push
}
