//
//  EarthquakeCoordinator.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import UIKit

final class EarthquakeCoordinator: Coordinator {
    
    internal var navigationController: UINavigationController
    internal var childCoordinators: [CoordinatorKeys:Coordinator]
    
    private let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    init(with navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = [:]
        start()
    }
    
    func start() {
        let mainViewController = MainViewController.instantiate()
        mainViewController.coordinator = self
        navigate(to: mainViewController, with: .push, animated: false)
    }
    
    func gotoEarthquakeListViewController(viewModel: EarthquakeListViewModel) {
        let earthquakeListViewController = EarthquakeListViewController.instantiate()
        earthquakeListViewController.coordinator = self
        earthquakeListViewController.viewModel = viewModel
        navigate(to: earthquakeListViewController, with: .push, animated: true)
    }
    
    func gotoGeneralEarthquakeMapViewController(viewModel: EarthquakeMapViewModel) {
        let earthquakeMapViewController = EarthquakeMapViewController.instantiate()
        earthquakeMapViewController.coordinator = self
        earthquakeMapViewController.viewModel = viewModel
        navigate(to: earthquakeMapViewController, with: .push, animated: true)
    }
    
    func gotoSingleEarthquakeMapViewController(viewModel: EarthquakeMapViewModel) {
        let earthquakeMapViewController = EarthquakeMapViewController.instantiate()
        earthquakeMapViewController.coordinator = self
        earthquakeMapViewController.viewModel = viewModel
        navigate(to: earthquakeMapViewController, with: .push, animated: true)
    }
}
