//
//  EarthquakeListTableViewCell.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import UIKit

class EarthquakeTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /// EarthquakeTableViewCell configuration
    ///
    /// - Parameter title: String to identify the earthquake place name
    func config(title: String?) {
        guard let _title = title else {
            return
        }
        titleLabel.text = _title
    }
}
