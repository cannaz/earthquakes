//
//  EarthquakeListViewController.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import UIKit

final class EarthquakeListViewController: UIViewController, Storyboarded, AlertProtocol {
    
    @IBOutlet private weak var tableView: UITableView!
    
    var viewModel: EarthquakeListViewModel!
    var coordinator: EarthquakeCoordinator!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.black
        
        return refreshControl
    }()

    //MARK: Lyfecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutSetup()
        delegateSetup()
        setClosures()
        
        //Trying a data request when the viewcontroller starts
        viewModel.getEarthquakes()
    }
    
    //MARK: - Layout setup
    
    private func layoutSetup() {
        tableView.addSubview(refreshControl)
    }
    
    //MARK: - Setup
    
    private func delegateSetup() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    //MARK: - Closures
    
    private func setClosures() {
        viewModel.earthquakesUpdateSuccessful = { [unowned self] in
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
        
        viewModel.earthquakesUpdateFailed = { [unowned self] error in
            print("error getting earthquakes data: \(error)")
            self.refreshControl.endRefreshing()
            let cancelActionAlert = UIAlertAction(title: "Ok", style: .default, handler: nil)
            self.showAlertWithTitle("Error", message: "It was no possible to get earthquakes data", actions: [cancelActionAlert])
        }
    }
    
    //Pull to refresh func
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        viewModel.getEarthquakes()
    }
    
}

//TableView DataSource
extension EarthquakeListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.earthquakesCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellsIdentifiers.earthquakeListTableViewCellIdentifier) as? EarthquakeTableViewCell else {
            fatalError("Unknown identifier")
        }
        
        let title = viewModel.getEarthquakePlaceAtIndex(indexPath.row)
        cell.config(title: title)
        
        return cell
    }
}

//TableView Delegates
extension EarthquakeListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let coordinate = viewModel.getLocationAtIndex(indexPath.row)
        let earthquakeMapViewModel = EarthquakeMapViewModel(apiNetwork: viewModel.apiNetwork, mapMode: .singleEarthquake, coordinate: coordinate)
        coordinator.gotoSingleEarthquakeMapViewController(viewModel: earthquakeMapViewModel)
    }
}

