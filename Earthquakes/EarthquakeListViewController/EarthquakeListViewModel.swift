//
//  EarthquakeListViewModel.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import Foundation

final class EarthquakeListViewModel {
    
    //ApiService
    var apiNetwork: ApiServiceProtocol
    //Earthquake Codable object
    var earthquakes: Earthquake?
    
    //Closure that update the viewController if the request is succesful
    var earthquakesUpdateSuccessful: (() -> Void)?
    //Closure that update the viewController if the request fails
    var earthquakesUpdateFailed: ((Error) -> Void)?
    
    
    /// EarthquakeListViewModel init
    ///
    /// - Parameter apiNetwork: ApiService instance injection
    init(apiNetwork: ApiServiceProtocol) {
        self.apiNetwork = apiNetwork
    }
    
    /// Start the request to obtain all the earthquakes information
    func getEarthquakes() {
        apiNetwork.getEarthquakes(route: APIRouter.last24hEarthquakes) { result in
            switch result {
            case .success(let value):
                self.earthquakes = value
                self.earthquakesUpdateSuccessful?()
            case .error(let error):
                if let _error = error {
                    print(_error)
                    self.earthquakesUpdateFailed?(_error)
                }
            }
        }
    }
    
    
    /// Get the earthquakes number
    ///
    /// - Returns: earthwuakes count
    func earthquakesCount() -> Int {
        guard let _earthquakes = earthquakes else {
            return 0
        }
        
        return _earthquakes.features.count
    }
    
    /// Get the single earthquake place name
    ///
    /// - Parameter index: features index array
    /// - Returns: String with the place name
    func getEarthquakePlaceAtIndex(_ index: Int) -> String? {
        guard let _earthquakes = earthquakes else {
            return nil
        }
        
        return _earthquakes.features[index].properties.place
    }
    
    
    /// Get the single earthquake coordinate information
    ///
    /// - Parameter index: features index array
    /// - Returns: Tuple with latitude and longitude params
    func getLocationAtIndex(_ index: Int) -> (latitude: Double, longitude: Double)? {
        guard let _earthquakes = earthquakes else {
            return nil
        }
        
        return (latitude: _earthquakes.features[index].geometry.latitude, longitude: _earthquakes.features[index].geometry.longitude)
    }
}
