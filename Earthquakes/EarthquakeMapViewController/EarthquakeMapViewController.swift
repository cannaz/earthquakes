//
//  EarthquakeMapViewController.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import UIKit
import MapKit

final class EarthquakeMapViewController: UIViewController, Storyboarded, AlertProtocol {
    
    @IBOutlet private weak var mapView: MKMapView!
    
    var viewModel: EarthquakeMapViewModel!
    var coordinator: EarthquakeCoordinator!
    
    //MARK: Lyfecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegates()
        setClosures()
        
        populateMap()
    }
    
    //MARK: - Setup
    
    private func setDelegates() {
        mapView.delegate = self
    }
    
    //MARK: - Closures
    
    private func setClosures() {
        viewModel.earthquakesUpdateSuccessful = { [unowned self] in
            self.addAnnotationsToTheMap()
        }
        
        viewModel.earthquakesUpdateFailed = { [unowned self] error in
            print("error getting earthquakes data: \(error)")
            let cancelActionAlert = UIAlertAction(title: "Ok", style: .default, handler: nil)
            self.showAlertWithTitle("Error", message: "It was no possible to get earthquakes data", actions: [cancelActionAlert])
        }
    }
    
    /// Start to populate the map
    private func populateMap() {
        if viewModel.mapMode == .allEarthquakes {
            viewModel.getEarthquakes()
        } else {
            if let _coordinate = viewModel.earthquakeCoordinate {
                let location = CLLocation(latitude: _coordinate.latitude, longitude: _coordinate.longitude)
                addSingleAnnotationToTheMap(location: location)
                mapView.showAnnotations(mapView.annotations, animated: true)
            }
        }
    }
    
    /// Adding all the earthquakes annotation to the map
    private func addAnnotationsToTheMap() {
        guard let _earthquakes = viewModel.earthquakes else {
            return
        }
        
        for feature in _earthquakes.features {
            let latitude = feature.geometry.latitude
            let longitude = feature.geometry.longitude
            let coordinate = CLLocation(latitude: latitude, longitude: longitude)
            let title = feature.properties.place
            var timeString: String?
            if let _time = feature.properties.time {
                timeString = EarthquakeUtility.getStringDateFromMilliseconds(_time)
            }
            addSingleAnnotationToTheMap(location: coordinate, title: title, subTitle: timeString)
        }
        
        mapView.showAnnotations(mapView.annotations, animated: true)
    }
    
    /// Adding just a single annotation to the map
    private func addSingleAnnotationToTheMap(location: CLLocation, title: String? = nil, subTitle: String? = nil) {
        let annotation = MKPointAnnotation()
        annotation.title = title
        annotation.subtitle = subTitle
        annotation.coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        mapView.addAnnotation(annotation)
    }

}

//MKMapView delegates
extension EarthquakeMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let identifier = MapAnnotationIdentifier.earthquakeMapAnnotation
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
    
}
