//
//  EarthquakeMapViewModel.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import Foundation


/// EartquakeMapViewModel Mode
///
/// - allEarthquakes: setup to show all the earthquake on the map
/// - singleEarthquake: setup to show only a single annotation on the map
enum MapMode {
    case allEarthquakes
    case singleEarthquake
}

final class EarthquakeMapViewModel {
    
    //ApiService
    var apiNetwork: ApiServiceProtocol
    //Earthquake Codable object
    var earthquakes: Earthquake?
    //MapMode
    var mapMode: MapMode
    //Single annotation coordinate 
    typealias Coordinate = (latitude: Double, longitude: Double)
    var earthquakeCoordinate: Coordinate?
    
    //Closure that update the viewController if the request is succesful
    var earthquakesUpdateSuccessful: (() -> Void)?
    //Closure that update the viewController if the request fails
    var earthquakesUpdateFailed: ((Error) -> Void)?
    
    /// EarthquakeMapViewModel init
    ///
    /// - Parameters:
    ///   - apiNetwork: ApiService instance injection
    ///   - mapMode: MapMode case
    ///   - coordinate: single annotation Coordinate
    init(apiNetwork: ApiServiceProtocol, mapMode: MapMode, coordinate: Coordinate? = nil) {
        self.apiNetwork = apiNetwork
        self.mapMode = mapMode
        self.earthquakeCoordinate = coordinate
    }
    
    /// Start the request to obtain all the earthquakes information
    func getEarthquakes() {
        apiNetwork.getEarthquakes(route: APIRouter.last24hEarthquakes) { result in
            switch result {
            case .success(let value):
                self.earthquakes = value
                self.earthquakesUpdateSuccessful?()
            case .error(let error):
                if let _error = error {
                    print(_error)
                    self.earthquakesUpdateFailed?(_error)
                }
            }
        }
    }
}
