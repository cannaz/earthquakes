//
//  MainViewController.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import UIKit

final class MainViewController: UIViewController, Storyboarded {
    
    @IBOutlet private weak var tableView: UITableView!
    
    var coordinator: EarthquakeCoordinator!
    
    lazy var apiService: ApiService = {
        return ApiService()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegateSetup()
        tableviewSetup()
    }
    
    private func delegateSetup() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func tableviewSetup() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellsIdentifiers.mainTableViewCell)
    }
    
}

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellsIdentifiers.mainTableViewCell) else {
            fatalError("Unknown identifier")
        }
        if indexPath.row == 0 {
            cell.textLabel?.text = "Earthquakes List"
        } else {
            cell.textLabel?.text = "Earthquakes Map"
        }
        return cell
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let earthquakeListViewModel = EarthquakeListViewModel(apiNetwork: apiService)
            coordinator.gotoEarthquakeListViewController(viewModel: earthquakeListViewModel)
        } else if indexPath.row == 1 {
            let earthquakeMapViewModel = EarthquakeMapViewModel(apiNetwork: apiService, mapMode: .allEarthquakes)
            coordinator.gotoGeneralEarthquakeMapViewController(viewModel: earthquakeMapViewModel)
        }
    }
}
