//
//  Earthquake.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import Foundation

/// General Earthquake Codable object
struct Earthquake: Codable {
    let type: String
    let metadata: Metadata
    let bbox: [Double]
    let features: [Features]
}

/// Earthquake Metadata
struct Metadata: Codable {
    let generated: Int
    let url: String
    let title: String
    let status: Int
    let api: String
    let count: Int
}

/// Earthquake Features: array with all the earthquakes data
struct Features: Codable {
    let type: String
    let properties: Properties
    let geometry: Geometry
    let id: String
}


/// Feature Properties
struct Properties: Codable {
    let mag: Double?
    let place: String?
    let time: Int?
    let updated: Int?
    let tz: Int?
    let url: String?
    let detail: String?
    let felt: Int?
    let cdi: Double?
    let mmi: Double?
    let alert: String?
    let status: String?
    let tsunami: Int?
    let sig: Int?
    let net: String?
    let code: String?
    let ids: String?
    let sources: String?
    let types: String?
    let nst: Int?
    let dmin: Double?
    let rms: Double?
    let gap: Double?
    let magType: String?
    let type: String?
}

/// Feature Geometry: coordinates data and depth
struct Geometry: Codable {
    let type: String
    let coordinates: [Double]
    
    var latitude: Double {
        return coordinates[1]
    }
    
    var longitude: Double {
        return coordinates[0]
    }
    
    var depth: Double {
        return coordinates[2]
    }
}


