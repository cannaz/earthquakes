//
//  EarthquakeUtility.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import Foundation

class EarthquakeUtility {
    /// Milliseconds to Date converter
    ///
    /// - Parameter millisecondsTimestamp: milliseconds
    /// - Returns: Date
    static func getStringDateFromMilliseconds(_ millisecondsTimestamp: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(millisecondsTimestamp/1000))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        return dateFormatter.string(from: date)
    }
}
