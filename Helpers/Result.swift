//
//  Result.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

enum Result<T> {
    case success(T)
    case error(Error?)
}
