//
//  UIAlertHelper.swift
//  Earthquakes
//
//  Created by Nazzareno Cantalamessa on 14/02/2019.
//  Copyright © 2019 Nazzareno. All rights reserved.
//

import UIKit

protocol AlertProtocol {
    func showAlertWithTitle(_ title: String?, message: String?, actions: [UIAlertAction])
}

extension AlertProtocol where Self: UIViewController {
    /// Generic func to show a UIAlertView
    ///
    /// - Parameters:
    ///   - title: alert title
    ///   - message: alert message
    ///   - actions: UIAlertAction array
    func showAlertWithTitle(_ title: String?, message: String?, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        for action in actions {
            alert.addAction(action)
        }
        present(alert, animated: true, completion: nil)
    }
}
